package main

import (
	"github.com/joho/godotenv"
	"go-grpc/gRPC_gateway/config"
	"go-grpc/gRPC_gateway/internal/infrastructure/logs"
	"go-grpc/gRPC_gateway/run"
	"os"
)

// @title           grpc microservices + exchange
// @version         2
// @description     jrpc + grpc
// @host      localhost:8080

// @securityDefinitions.apiKey ApiKeyAuth
// @in header
// @name Authorization

func main() {
	// Загружаем переменные окружения из файла .env
	err := godotenv.Load()
	// Создаем конфигурацию приложения
	conf := config.NewAppConf()
	// Создаем логгер
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}

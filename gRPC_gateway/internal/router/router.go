package router

import (
	"github.com/go-chi/chi/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	"go-grpc/gRPC_gateway/internal/infrastructure/component"
	"go-grpc/gRPC_gateway/internal/infrastructure/middleware"
	"go-grpc/gRPC_gateway/internal/modules"
	_ "go-grpc/gRPC_gateway/static" // Импортируйте пакет docs, чтобы Swaggo сгенерировал документацию
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	r.Get("/swagger/*", httpSwagger.Handler(httpSwagger.URL("http://localhost:8080/docs/doc.json")))
	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/auth", func(r chi.Router) {
				authController := controllers.Auth
				r.Post("/register", authController.Register)
				r.Post("/login", authController.Login)
				r.Route("/refresh", func(r chi.Router) {
					r.Use(authCheck.CheckRefresh)
					r.Post("/", authController.Refresh)
				})
				r.Post("/verify", authController.Verify)
			})
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
				})
			})
			r.Route("/exchange", func(r chi.Router) {
				exchangeControllers := controllers.Exchange
				r.Use(authCheck.CheckStrict)
				r.Get("/min", exchangeControllers.MinPrices)
				r.Get("/max", exchangeControllers.MaxPrices)
				r.Get("/avg", exchangeControllers.AveragePrices)
				r.Get("/history", exchangeControllers.History)
			})

		})
	})

	return r
}

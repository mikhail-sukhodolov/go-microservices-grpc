package controller

import (
	"github.com/ptflp/godecoder"
	"go-grpc/gRPC_gateway/internal/infrastructure/component"
	"go-grpc/gRPC_gateway/internal/infrastructure/errors"
	"go-grpc/gRPC_gateway/internal/infrastructure/handlers"
	"go-grpc/gRPC_gateway/internal/infrastructure/responder"
	"go-grpc/gRPC_gateway/internal/modules/auth/service"
	"net/http"
	"net/mail"
)

type Auther interface {
	Register(http.ResponseWriter, *http.Request)
	Login(http.ResponseWriter, *http.Request)
	Refresh(w http.ResponseWriter, r *http.Request)
	Verify(w http.ResponseWriter, r *http.Request)
}

type Auth struct {
	authService service.Auther
	responder.Responder
	godecoder.Decoder
}

func NewAuth(service service.Auther, components *component.Components) Auther {
	return &Auth{authService: service, Responder: components.Responder, Decoder: components.Decoder}
}

func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

// Register
// @Summary      Register
// @Description  Register new user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param   input   body   controller.RegisterRequest   true   "JSON object"
// @Failure      500
// @Success      200 {object} controller.RegisterResponse
// @Router       /api/1/auth/register [post]
func (a *Auth) Register(w http.ResponseWriter, r *http.Request) {
	var req RegisterRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}

	if !valid(req.Email) {
		a.OutputJSON(w, RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "invalid email",
			},
		})
		return
	}

	if req.Password != req.RetypePassword {
		a.OutputJSON(w, RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "passwords mismatch",
			},
		})
		return
	}

	out := a.authService.Register(r.Context(), service.RegisterIn{
		Email:    req.Email,
		Password: req.Password,
	})

	if out.ErrorCode != errors.NoError {
		msg := "register error"
		if out.ErrorCode == errors.UserServiceUserAlreadyExists {
			msg = "User already exists, please check your email"
		}
		a.OutputJSON(w, RegisterResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: msg,
			},
		})
		return
	}

	a.OutputJSON(w, RegisterResponse{
		Success: true,
		Data: Data{
			Message: "verification link sent to " + req.Email,
		},
	})
}

// Login
// @Summary      Login
// @Description  Login user
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param   input   body   controller.LoginRequest   true   "JSON object"
// @Failure      500
// @Success      200 {object} controller.LoginData
// @Router       /api/1/auth/login [post]
func (a *Auth) Login(w http.ResponseWriter, r *http.Request) {
	var req LoginRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "phone or email empty",
			},
		})
	}

	out := a.authService.AuthorizeEmail(r.Context(), service.AuthorizeEmailIn{
		Email:    req.Email,
		Password: req.Password,
	})
	if out.ErrorCode == errors.AuthServiceUserNotVerified {
		a.OutputJSON(w, AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: LoginData{
				Message: "user email is not verified",
			},
		})
		return
	}

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, AuthResponse{
		Success: true,
		Data: LoginData{
			Message:      "success login",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

// Refresh
// @Summary      Refresh
// @Description  Refresh token
// @Security ApiKeyAuth
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param   input   body   controller.RefreshRequest   true   "JSON object"
// @Failure      500
// @Success      200 {object} controller.LoginData
// @Router       /api/1/auth/refresh [post]
func (a *Auth) Refresh(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	out := a.authService.AuthorizeRefresh(r.Context(), service.AuthorizeRefreshIn{UserID: claims.ID})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, AuthResponse{
		Success: true,
		Data: LoginData{
			Message:      "success refresh",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

// Verify
// @Summary      Verify
// @Description  Verify email
// @Security ApiKeyAuth
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param   input   body   controller.VerifyRequest   true   "JSON object"
// @Failure      500
// @Success      200 {object} controller.VerifyResponse
// @Router       /api/1/auth/verify  [post]
func (a *Auth) Verify(w http.ResponseWriter, r *http.Request) {
	var req VerifyRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: Data{
				Message: "phone or email empty",
			},
		})
	}

	out := a.authService.VerifyEmail(r.Context(), service.VerifyEmailIn{
		Email: req.Email,
		Hash:  req.Hash,
	})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, AuthResponse{
		Success: true,
		Data: LoginData{
			Message: "email verification success",
		},
	})
}

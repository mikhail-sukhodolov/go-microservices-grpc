package service

import (
	"context"
	grpcService "go-grpc/gRPC_gateway/rpc/auth/grpc"
	"google.golang.org/grpc"
)

// в клиенте интерфейс из прото файла
type AuthServiceGRPC struct {
	client grpcService.AutherClient
}

func NewAuthServiceGRPC(client grpc.ClientConnInterface) *AuthServiceGRPC {
	newClient := grpcService.NewAutherClient(client)
	return &AuthServiceGRPC{client: newClient}
}

func (a *AuthServiceGRPC) Register(ctx context.Context, in RegisterIn) RegisterOut {
	out, err := a.client.Register(ctx, &grpcService.RegisterIn{
		Email:          in.Email,
		Phone:          in.Phone,
		Password:       in.Password,
		IdempotencyKey: in.IdempotencyKey,
	})
	if err != nil {
		return RegisterOut{ErrorCode: int(out.ErrorCode)}
	}

	return RegisterOut{
		Status:    int(out.Status),
		ErrorCode: int(out.ErrorCode),
	}
}

func (a *AuthServiceGRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	out, err := a.client.AuthorizeEmail(ctx, &grpcService.AuthorizeEmailIn{
		Email:             in.Email,
		Password:          in.Password,
		RemessagePassword: in.RetypePassword,
	})
	if err != nil {
		return AuthorizeOut{ErrorCode: int(out.ErrorCode)}
	}
	return AuthorizeOut{
		UserID:       int(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int(out.ErrorCode),
	}
}

func (a *AuthServiceGRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	out, err := a.client.AuthorizeRefresh(ctx, &grpcService.AuthorizeRefreshIn{UserID: int32(in.UserID)})
	if err != nil {
		return AuthorizeOut{ErrorCode: int(out.ErrorCode)}
	}
	return AuthorizeOut{
		UserID:       int(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int(out.ErrorCode),
	}
}

func (a *AuthServiceGRPC) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	out, err := a.client.AuthorizePhone(ctx, &grpcService.AuthorizePhoneIn{
		Phone: in.Phone,
		Code:  int32(in.Code),
	})
	if err != nil {
		return AuthorizeOut{ErrorCode: int(out.ErrorCode)}
	}
	return AuthorizeOut{
		UserID:       int(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int(out.ErrorCode),
	}
}

func (a *AuthServiceGRPC) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	out, err := a.client.SendPhoneCode(ctx, &grpcService.SendPhoneCodeIn{Phone: in.Phone})
	if err != nil {
		return SendPhoneCodeOut{Code: int(out.Code)}
	}
	return SendPhoneCodeOut{
		Code:  int(out.Code),
		Phone: out.Phone,
	}
}

func (a *AuthServiceGRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	out, err := a.client.VerifyEmail(ctx, &grpcService.VerifyEmailIn{
		Hash:  in.Hash,
		Email: in.Email,
	})
	if err != nil {
		return VerifyEmailOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	return VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: int(out.ErrorCode),
	}
}

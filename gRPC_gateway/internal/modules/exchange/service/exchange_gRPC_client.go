package service

import (
	"context"
	grpcService "go-grpc/gRPC_gateway/rpc/exchange/grpc"
	"google.golang.org/grpc"
)

type ExchangerServiceGRPCClient interface {
	Ticker(ctx context.Context, in *grpcService.TickerRequest, opts ...grpc.CallOption) (*grpcService.TickerResponse, error)
	Update(ctx context.Context, in *grpcService.UpdateRequest, opts ...grpc.CallOption) (*grpcService.UpdateResponse, error)
	MinPrices(ctx context.Context, in *grpcService.MinPricesRequest, opts ...grpc.CallOption) (*grpcService.MinPricesResponse, error)
	MaxPrices(ctx context.Context, in *grpcService.MaxPricesRequest, opts ...grpc.CallOption) (*grpcService.MaxPricesResponse, error)
	AvgPrices(ctx context.Context, in *grpcService.AvgPricesRequest, opts ...grpc.CallOption) (*grpcService.AvgPricesResponse, error)
	History(ctx context.Context, in *grpcService.HistoryRequest, opts ...grpc.CallOption) (*grpcService.HistoryResponse, error)
}

type exchangerServiceGRPCClient struct {
	cc grpc.ClientConnInterface
}

func NewExchangerServiceGRPCClient(cc grpc.ClientConnInterface) ExchangerServiceGRPCClient {
	return &exchangerServiceGRPCClient{cc}
}

func (c *exchangerServiceGRPCClient) Ticker(ctx context.Context, in *grpcService.TickerRequest, opts ...grpc.CallOption) (*grpcService.TickerResponse, error) {
	out := new(grpcService.TickerResponse)
	err := c.cc.Invoke(ctx, "/exchange.ExchangerServiceGRPC/Ticker", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangerServiceGRPCClient) Update(ctx context.Context, in *grpcService.UpdateRequest, opts ...grpc.CallOption) (*grpcService.UpdateResponse, error) {
	out := new(grpcService.UpdateResponse)
	err := c.cc.Invoke(ctx, "/exchange.ExchangerServiceGRPC/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangerServiceGRPCClient) MinPrices(ctx context.Context, in *grpcService.MinPricesRequest, opts ...grpc.CallOption) (*grpcService.MinPricesResponse, error) {
	out := new(grpcService.MinPricesResponse)
	err := c.cc.Invoke(ctx, "/exchange.ExchangerServiceGRPC/MinPrices", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangerServiceGRPCClient) MaxPrices(ctx context.Context, in *grpcService.MaxPricesRequest, opts ...grpc.CallOption) (*grpcService.MaxPricesResponse, error) {
	out := new(grpcService.MaxPricesResponse)
	err := c.cc.Invoke(ctx, "/exchange.ExchangerServiceGRPC/MaxPrices", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangerServiceGRPCClient) AvgPrices(ctx context.Context, in *grpcService.AvgPricesRequest, opts ...grpc.CallOption) (*grpcService.AvgPricesResponse, error) {
	out := new(grpcService.AvgPricesResponse)
	err := c.cc.Invoke(ctx, "/exchange.ExchangerServiceGRPC/AvgPrices", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *exchangerServiceGRPCClient) History(ctx context.Context, in *grpcService.HistoryRequest, opts ...grpc.CallOption) (*grpcService.HistoryResponse, error) {
	out := new(grpcService.HistoryResponse)
	err := c.cc.Invoke(ctx, "/exchange.ExchangerServiceGRPC/History", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

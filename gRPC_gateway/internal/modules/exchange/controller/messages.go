package controller

import "go-grpc/gRPC_gateway/rpc/exchange/grpc"

//go:generate easytags $GOFILE

type ProfileResponseMin struct {
	Success   bool    `json:"success"`
	ErrorCode int     `json:"error_code,omitempty"`
	Data      DataMin `json:"data"`
}

type DataMin struct {
	Message string           `json:"message,omitempty"`
	Worker  []*grpc.MinPrice `json:"worker,omitempty"`
}

type ProfileResponseMax struct {
	Success   bool    `json:"success"`
	ErrorCode int     `json:"error_code,omitempty"`
	Data      DataMax `json:"data"`
}

type DataMax struct {
	Message string           `json:"message,omitempty"`
	Worker  []*grpc.MaxPrice `json:"worker,omitempty"`
}

type ProfileResponseAvg struct {
	Success   bool    `json:"success"`
	ErrorCode int     `json:"error_code,omitempty"`
	Data      DataAvg `json:"data"`
}

type DataAvg struct {
	Message string           `json:"message,omitempty"`
	Worker  []*grpc.AvgPrice `json:"worker,omitempty"`
}

type ProfileResponseHistory struct {
	Success   bool        `json:"success"`
	ErrorCode int         `json:"error_code,omitempty"`
	Data      DataHistory `json:"data"`
}

type DataHistory struct {
	Message string              `json:"message,omitempty"`
	Worker  []*grpc.ExchangeDTO `json:"worker,omitempty"`
}

type ChangePasswordRequest struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

type ChangePasswordResponse struct {
	Success   bool   `json:"success"`
	ErrorCode int    `json:"error_code,omitempty"`
	Message   string `json:"message"`
}

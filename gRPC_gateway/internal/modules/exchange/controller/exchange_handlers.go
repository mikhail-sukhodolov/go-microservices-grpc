package controller

import (
	"github.com/ptflp/godecoder"
	"go-grpc/gRPC_gateway/internal/infrastructure/component"
	"go-grpc/gRPC_gateway/internal/infrastructure/responder"
	"go-grpc/gRPC_gateway/internal/modules/exchange/service"
	grpcService "go-grpc/gRPC_gateway/rpc/exchange/grpc"
	"net/http"
)

type Exchanger interface {
	MinPrices(w http.ResponseWriter, r *http.Request)
	MaxPrices(w http.ResponseWriter, r *http.Request)
	AveragePrices(w http.ResponseWriter, r *http.Request)
	History(w http.ResponseWriter, r *http.Request)
}

type Exchange struct {
	service service.ExchangerServiceGRPCClient
	responder.Responder
	godecoder.Decoder
}

func NewExchange(service service.ExchangerServiceGRPCClient, components *component.Components) Exchanger {
	return &Exchange{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

// History
// @Summary      History
// @Description  History exchange
// @Security ApiKeyAuth
// @Tags         exchange
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []controller.ProfileResponseHistory
// @Router       /api/1/exchange/history [get]
func (e *Exchange) History(w http.ResponseWriter, r *http.Request) {
	// создаем gRPC запрос
	request := &grpcService.HistoryRequest{}
	// выполняем вызов сервиса для получения истории
	response, err := e.service.History(r.Context(), request)
	if err != nil {
		e.OutputJSON(w, ProfileResponseHistory{
			Success:   response.Success,
			ErrorCode: int(response.ErrorCode),
			Data:      DataHistory{Message: "history error"},
		})
		return
	}
	// Выводим успешный результат
	e.OutputJSON(w, ProfileResponseHistory{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
		Data:      DataHistory{Worker: response.ExchangeDTOs},
	})
}

// MinPrices
// @Summary      MinPrices
// @Description  MinPrices rates
// @Security ApiKeyAuth
// @Tags         exchange
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []controller.ProfileResponseMin
// @Router       /api/1/exchange/min [get]
func (e *Exchange) MinPrices(w http.ResponseWriter, r *http.Request) {
	// cоздаем gRPC запрос
	request := &grpcService.MinPricesRequest{}
	// выполняем вызов сервиса
	response, err := e.service.MinPrices(r.Context(), request)
	if err != nil {
		e.OutputJSON(w, ProfileResponseMin{
			Success:   response.Success,
			ErrorCode: int(response.ErrorCode),
			Data:      DataMin{Message: "min price error"},
		})
		return
	}
	e.OutputJSON(w, ProfileResponseMin{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
		Data:      DataMin{Worker: response.Rates},
	})
}

// MaxPrices
// @Summary      MaxPrices
// @Description  MaxPrices rates
// @Security ApiKeyAuth
// @Tags         exchange
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []controller.ProfileResponseMax
// @Router       /api/1/exchange/max [get]
func (e *Exchange) MaxPrices(w http.ResponseWriter, r *http.Request) {
	// cоздаем gRPC запрос
	request := &grpcService.MaxPricesRequest{}
	// выполняем вызов сервиса
	response, err := e.service.MaxPrices(r.Context(), request)
	if err != nil {
		e.OutputJSON(w, ProfileResponseMax{
			Success:   response.Success,
			ErrorCode: int(response.ErrorCode),
			Data:      DataMax{Message: "max price error"},
		})
		return
	}
	e.OutputJSON(w, ProfileResponseMax{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
		Data:      DataMax{Worker: response.Rates},
	})
}

// AveragePrices
// @Summary      AveragePrices
// @Description  AveragePrices rates
// @Security ApiKeyAuth
// @Tags         exchange
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []controller.ProfileResponseAvg
// @Router       /api/1/exchange/avg [get]
func (e *Exchange) AveragePrices(w http.ResponseWriter, r *http.Request) {
	// cоздаем gRPC запрос
	request := &grpcService.AvgPricesRequest{}
	// выполняем вызов сервиса
	response, err := e.service.AvgPrices(r.Context(), request)
	if err != nil {
		e.OutputJSON(w, ProfileResponseAvg{
			Success:   response.Success,
			ErrorCode: int(response.ErrorCode),
			Data:      DataAvg{Message: "avg price error"},
		})
		return
	}
	e.OutputJSON(w, ProfileResponseAvg{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
		Data:      DataAvg{Worker: response.Rates},
	})
}

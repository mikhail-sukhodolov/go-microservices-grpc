package modules

import (
	aservice "go-grpc/gRPC_gateway/internal/modules/auth/service"
	eservice "go-grpc/gRPC_gateway/internal/modules/exchange/service"
	uservice "go-grpc/gRPC_gateway/internal/modules/user/service"
)

type Services struct {
	User     uservice.Userer
	Auth     aservice.Auther
	Exchange eservice.ExchangerServiceGRPCClient
}

func NewServices(userService uservice.Userer, authService aservice.Auther, exchangeService eservice.ExchangerServiceGRPCClient) *Services {
	return &Services{
		User:     userService,
		Auth:     authService,
		Exchange: exchangeService,
	}
}

package controller

import (
	"github.com/ptflp/godecoder"
	"go-grpc/gRPC_gateway/internal/infrastructure/component"
	"go-grpc/gRPC_gateway/internal/infrastructure/errors"
	"go-grpc/gRPC_gateway/internal/infrastructure/handlers"
	"go-grpc/gRPC_gateway/internal/infrastructure/responder"
	"go-grpc/gRPC_gateway/internal/modules/user/service"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

// Profile
// @Summary      Profile
// @Description  get profile
// @Security ApiKeyAuth
// @Tags         user
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} models.User
// @Router       /api/1/user/profile [get]
func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

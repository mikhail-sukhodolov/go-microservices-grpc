package service

import (
	"context"
	"go-grpc/gRPC_gateway/internal/models"
	grpcService "go-grpc/gRPC_gateway/rpc/user/grpc"
	"google.golang.org/grpc"
	"unsafe"
)

type UserServiceGRPC struct {
	client grpcService.UsersClient
}

// в клиенте интерфейс из прото файла

func NewUserServiceGRPC(client grpc.ClientConnInterface) *UserServiceGRPC {
	newClient := grpcService.NewUsersClient(client)
	return &UserServiceGRPC{client: newClient}
}

func (u *UserServiceGRPC) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	out, err := u.client.CreateUser(ctx, &grpcService.UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int32(in.Role),
	})
	if err != nil {
		return UserCreateOut{
			UserID:    int(out.UserID),
			ErrorCode: int(out.ErrorCode),
		}
	}
	return UserCreateOut{
		UserID:    int(out.UserID),
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	fields := make([]int32, len(in.Fields))
	copy(fields, *(*[]int32)(unsafe.Pointer(&in.Fields)))
	out, err := u.client.Update(ctx, &grpcService.UserUpdateIn{
		User: &grpcService.User{
			ID:            int32(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int32(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: fields,
	})
	if err != nil {
		return UserUpdateOut{
			Success:   out.Success,
			ErrorCode: int(out.ErrorCode),
		}
	}
	return UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	out, err := u.client.VerifyEmail(ctx, &grpcService.UserVerifyEmailIn{UserID: int32(in.UserID)})
	if err != nil {
		return UserUpdateOut{
			Success:   out.Success,
			ErrorCode: int(out.ErrorCode)}
	}
	return UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	out, err := u.client.ChangePassword(ctx, &grpcService.ChangePasswordIn{
		UserID:      int32(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	})
	if err != nil {
		return ChangePasswordOut{
			Success:   out.Success,
			ErrorCode: int(out.ErrorCode),
		}
	}
	return ChangePasswordOut{
		Success:   out.Success,
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	out, err := u.client.GetByEmail(ctx, &grpcService.GetByEmailIn{Email: in.Email})
	if err != nil {
		return UserOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	return UserOut{
		User: &models.User{
			ID:            int(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	out, err := u.client.GetByPhone(ctx, &grpcService.GetByPhoneIn{Phone: in.Phone})
	if err != nil {
		return UserOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	return UserOut{
		User: &models.User{
			ID:    int(out.User.ID),
			Name:  out.User.Name,
			Phone: out.User.Phone,
			Email: out.User.Email,
			Role:  int(out.User.Role),
		},
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	out, err := u.client.GetByID(ctx, &grpcService.GetByIDIn{UserID: int32(in.UserID)})
	if err != nil {
		return UserOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	return UserOut{
		User: &models.User{
			ID:    int(out.User.ID),
			Name:  out.User.Name,
			Phone: out.User.Phone,
			Email: out.User.Email,
			Role:  int(out.User.Role),
		},
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	IDs := *(*[]int32)(unsafe.Pointer(&in.UserIDs))
	out, err := u.client.GetByIDs(ctx, &grpcService.GetByIDsIn{UserIDs: IDs})
	if err != nil {
		return UsersOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	users := make([]models.User, len(out.User))
	for index, user := range out.User {
		users[index] = models.User{
			ID:            int(user.ID),
			Email:         user.Email,
			Password:      user.Password,
			Name:          user.Name,
			Phone:         user.Phone,
			EmailVerified: user.EmailVerified,
			PhoneVerified: user.PhoneVerified,
			Verified:      user.Verified,
			Role:          int(user.Role),
		}
	}
	return UsersOut{
		User:      users,
		ErrorCode: int(out.ErrorCode),
	}
}

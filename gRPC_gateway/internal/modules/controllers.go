package modules

import (
	"go-grpc/gRPC_gateway/internal/infrastructure/component"
	acontroller "go-grpc/gRPC_gateway/internal/modules/auth/controller"
	econtroller "go-grpc/gRPC_gateway/internal/modules/exchange/controller"
	ucontroller "go-grpc/gRPC_gateway/internal/modules/user/controller"
)

type Controllers struct {
	Auth     acontroller.Auther
	User     ucontroller.Userer
	Exchange econtroller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)
	exchangeController := econtroller.NewExchange(services.Exchange, components)

	return &Controllers{
		Auth:     authController,
		User:     userController,
		Exchange: exchangeController,
	}
}

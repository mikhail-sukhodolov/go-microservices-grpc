package rabbit

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/streadway/amqp"
	"go-grpc/gRPC_gateway/config"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

type RabbitServer struct {
	conf   config.AppConf
	logger zap.Logger
}

func NewRabbitServer(conf config.AppConf, logger zap.Logger) *RabbitServer {
	return &RabbitServer{conf: conf, logger: logger}
}

func (r *RabbitServer) Run() {
	conn, err := amqp.Dial("amqp://guest:guest@" + r.conf.RabbitServer.Host + ":" + r.conf.RabbitServer.Port)
	if err != nil {
		r.logger.Error("Failed to connect to RabbitMQ", zap.Error(err))
		return
	}
	defer conn.Close()

	r.logger.Info("Rabbit server started", zap.String("port", r.conf.RabbitServer.Port))

	ch, err := conn.Channel()
	if err != nil {
		r.logger.Error("Failed to open a channel", zap.Error(err))
		return
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		r.logger.Error("Failed to declare a queue", zap.Error(err))
		return
	}

	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		r.logger.Error("Failed to register a consumer", zap.Error(err))
		return
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		for d := range msgs {
			r.logger.Info("Received a message", zap.ByteString("body", d.Body))
			chatID, err := strconv.ParseInt(r.conf.TelegramBot.ChatID, 10, 64)
			if err != nil {
				r.logger.Error("parse int error")
			}
			r.SendTelegramMessage(r.conf.TelegramBot.BotToken, chatID, string(d.Body))

		}
	}()

	<-stop
	r.logger.Info("Rabbit server stopping gracefully...")
}

func (r *RabbitServer) SendTelegramMessage(botToken string, chatID int64, message string) error {
	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		return err
	}

	msg := tgbotapi.NewMessage(chatID, message)

	_, err = bot.Send(msg)
	return err
}

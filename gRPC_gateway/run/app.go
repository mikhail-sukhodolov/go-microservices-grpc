package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"go-grpc/gRPC_gateway/config"
	"go-grpc/gRPC_gateway/internal/infrastructure/component"
	"go-grpc/gRPC_gateway/internal/infrastructure/errors"
	"go-grpc/gRPC_gateway/internal/infrastructure/rabbit"
	"go-grpc/gRPC_gateway/internal/infrastructure/responder"
	"go-grpc/gRPC_gateway/internal/infrastructure/router"
	"go-grpc/gRPC_gateway/internal/infrastructure/server"
	internal "go-grpc/gRPC_gateway/internal/infrastructure/service"
	"go-grpc/gRPC_gateway/internal/infrastructure/tools/cryptography"
	"go-grpc/gRPC_gateway/internal/modules"
	aservice "go-grpc/gRPC_gateway/internal/modules/auth/service"
	eservice "go-grpc/gRPC_gateway/internal/modules/exchange/service"
	uservice "go-grpc/gRPC_gateway/internal/modules/user/service"

	"go-grpc/gRPC_gateway/internal/provider"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"net/http"
	"net/rpc/jsonrpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	Sig      chan os.Signal
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)

	var userClientRPC uservice.Userer
	var authClientRPC aservice.Auther
	var exchangeClientRPC eservice.ExchangerServiceGRPCClient

	switch a.conf.RPCServer.Type {
	case "jsonRPC":
		// инициализация клиента для взаимодействия с сервисом пользователей
		clientUser, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))
		if err != nil {
			a.logger.Fatal("error init user rpc client", zap.Error(err))
		}
		a.logger.Info("rpc client connected")
		userClientRPC = uservice.NewUserServiceJSONRPC(clientUser)

		// инициализация клиента для взаимодействия с сервисом аутентификации
		clientAuth, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.AuthRPC.Host, a.conf.AuthRPC.Port))
		if err != nil {
			a.logger.Fatal("error init auth rpc client", zap.Error(err))
		}
		a.logger.Info("rpc client connected")
		authClientRPC = aservice.NewAuthServiceJSONRPC(clientAuth)

	case "gRPC":
		// инициализация клиента для взаимодействия с сервисом аутентификации
		authClient, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.AuthGRPC.Host, a.conf.AuthGRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init grpc auth client", zap.Error(err))
		}
		a.logger.Info("grpc auth client connected", zap.String("port", a.conf.AuthGRPC.Port))
		authClientRPC = aservice.NewAuthServiceGRPC(authClient)

		// инициализация клиента для взаимодействия с сервисом пользователей
		userClient, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.UserGRPC.Host, a.conf.UserGRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init grpc user client", zap.Error(err))
		}
		a.logger.Info("grpc user client connected", zap.String("port", a.conf.UserGRPC.Port))
		userClientRPC = uservice.NewUserServiceGRPC(userClient)
	}
	//exchange GRPC client
	exchangeClient, err := grpc.Dial(fmt.Sprintf("%s:%s", a.conf.ExchangeGRPC.Host, a.conf.ExchangeGRPC.Port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		a.logger.Fatal("error init grpc exchange client", zap.Error(err))
	}
	a.logger.Info("grpc exchange client connected", zap.String("port", a.conf.ExchangeGRPC.Port))
	exchangeClientRPC = eservice.NewExchangerServiceGRPCClient(exchangeClient)

	// инициализация сервисов
	services := modules.NewServices(userClientRPC, authClientRPC, exchangeClientRPC)
	a.Servises = services
	//инициализация хендлеров
	controllers := modules.NewControllers(services, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)

	//запускаем сервер rabbit
	rabbitServer := rabbit.NewRabbitServer(a.conf, *a.logger)
	rabbitServer.Run()

	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)

	// возвращаем приложение
	return a
}

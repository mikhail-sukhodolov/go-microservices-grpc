package storages

import (
	"go-grpc/gRPC_user/internal/db/adapter"
	"go-grpc/gRPC_user/internal/infrastructure/cache"
	vstorage "go-grpc/gRPC_user/internal/modules/auth/storage"
	ustorage "go-grpc/gRPC_user/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}

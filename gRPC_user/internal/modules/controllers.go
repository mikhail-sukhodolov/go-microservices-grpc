package modules

import (
	"go-grpc/gRPC_user/internal/infrastructure/component"
	ucontroller "go-grpc/gRPC_user/internal/modules/user/controller"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		User: userController,
	}
}

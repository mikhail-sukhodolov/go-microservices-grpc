package modules

import (
	"go-grpc/gRPC_user/internal/infrastructure/component"
	uservice "go-grpc/gRPC_user/internal/modules/user/service"
	"go-grpc/gRPC_user/internal/storages"
)

type Services struct {
	User uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		User: uservice.NewUserService(storages.User, components.Logger),
	}
}

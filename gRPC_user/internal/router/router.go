package router

import (
	"github.com/go-chi/chi/v5"
	"go-grpc/gRPC_user/internal/infrastructure/component"
	"go-grpc/gRPC_user/internal/infrastructure/middleware"
	"go-grpc/gRPC_user/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
				})
			})
		})
	})

	return r
}

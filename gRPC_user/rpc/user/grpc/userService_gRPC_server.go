package grpc

import (
	"context"
	"go-grpc/gRPC_user/internal/models"
	"go-grpc/gRPC_user/internal/modules/user/service"
)

type UserServiceGRPC struct {
	user service.Userer
}

func NewUserServiceGRPC(userService service.Userer) *UserServiceGRPC {
	return &UserServiceGRPC{user: userService}
}

func (u *UserServiceGRPC) CreateUser(ctx context.Context, in *UserCreateIn) (*UserCreateOut, error) {
	out := u.user.Create(ctx, service.UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int(in.Role),
	})

	return &UserCreateOut{
		UserID:    int32(out.UserID),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) Update(ctx context.Context, in *UserUpdateIn) (*UserUpdateOut, error) {
	out := u.user.Update(ctx, service.UserUpdateIn{
		User: models.User{
			ID:            int(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: nil,
	})

	return &UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) VerifyEmail(ctx context.Context, in *UserVerifyEmailIn) (*UserUpdateOut, error) {
	out := u.user.VerifyEmail(ctx, service.UserVerifyEmailIn{UserID: int(in.UserID)})
	return &UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) ChangePassword(ctx context.Context, in *ChangePasswordIn) (*ChangePasswordOut, error) {
	out := u.user.ChangePassword(ctx, service.ChangePasswordIn{
		UserID:      int(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	})

	return &ChangePasswordOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByEmail(ctx context.Context, in *GetByEmailIn) (*UserOut, error) {
	out := u.user.GetByEmail(ctx, service.GetByEmailIn{Email: in.Email})

	return &UserOut{
		User: &User{
			ID:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByPhone(ctx context.Context, in *GetByPhoneIn) (*UserOut, error) {
	out := u.user.GetByPhone(ctx, service.GetByPhoneIn{Phone: in.Phone})
	return &UserOut{
		User: &User{
			ID:    int32(out.User.ID),
			Name:  out.User.Name,
			Phone: out.User.Phone,
			Email: out.User.Email,
			Role:  int32(out.User.Role),
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByID(ctx context.Context, in *GetByIDIn) (*UserOut, error) {
	out := u.user.GetByID(ctx, service.GetByIDIn{UserID: int(in.UserID)})

	return &UserOut{
		User: &User{
			ID:    int32(out.User.ID),
			Name:  out.User.Name,
			Phone: out.User.Phone,
			Email: out.User.Email,
			Role:  int32(out.User.Role),
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) GetByIDs(ctx context.Context, in *GetByIDsIn) (*UsersOut, error) {

	userIDs := make([]int, len(in.UserIDs))
	for i, v := range in.UserIDs {
		userIDs[i] = int(v)
	}

	out := u.user.GetByIDs(ctx, service.GetByIDsIn{UserIDs: userIDs})

	users := make([]*User, len(out.User))
	for index, user := range out.User {
		users[index] = &User{
			ID:            int32(user.ID),
			Email:         user.Email,
			Password:      user.Password,
			Name:          user.Name,
			Phone:         user.Phone,
			EmailVerified: user.EmailVerified,
			PhoneVerified: user.PhoneVerified,
			Verified:      user.Verified,
			Role:          int32(user.Role),
		}
	}
	return &UsersOut{
		User:      users,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u *UserServiceGRPC) mustEmbedUnimplementedUsersServer() {
	//TODO implement me
	panic("implement me")
}

package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"go-grpc/gRPC_user/config"
	"go-grpc/gRPC_user/internal/db"
	"go-grpc/gRPC_user/internal/infrastructure/cache"
	"go-grpc/gRPC_user/internal/infrastructure/component"
	"go-grpc/gRPC_user/internal/infrastructure/db/migrate"
	"go-grpc/gRPC_user/internal/infrastructure/db/scanner"
	"go-grpc/gRPC_user/internal/infrastructure/errors"
	"go-grpc/gRPC_user/internal/infrastructure/responder"
	"go-grpc/gRPC_user/internal/infrastructure/router"
	"go-grpc/gRPC_user/internal/infrastructure/server"
	internal "go-grpc/gRPC_user/internal/infrastructure/service"
	"go-grpc/gRPC_user/internal/infrastructure/tools/cryptography"
	"go-grpc/gRPC_user/internal/models"
	"go-grpc/gRPC_user/internal/modules"
	"go-grpc/gRPC_user/internal/provider"
	"go-grpc/gRPC_user/internal/storages"
	"go-grpc/gRPC_user/rpc/user"
	grpcService "go-grpc/gRPC_user/rpc/user/grpc"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net/http"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	gRPC     server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	switch a.conf.RPCServer.Type {
	case "jsonRPC":
		// запускаем json rpc сервер
		errGroup.Go(func() error {
			err := a.jsonRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("app: json server error", zap.Error(err))
				return err
			}
			return nil
		})
	case "gRPC":
		// запускаем созданный конструктором NewServerGRPC пользовательский сервер через метод Serve интерфейса Server
		errGroup.Go(func() error {
			err := a.gRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("app: json server error", zap.Error(err))
				return err
			}
			return nil
		})
	}

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}
	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.UserDTO{},
		&models.EmailVerifyDTO{},
		&models.TestDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}
	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Servises = services

	switch a.conf.RPCServer.Type {
	case "jsonRPC":
		// инициализация сервиса User в json RPC
		userRPC := user.NewUserServiceJSONRPC(services.User)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(userRPC)
		if err != nil {
			a.logger.Fatal("error init user json RPC", zap.Error(err))
		}
		// инициализация jsonRPC сервера
		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)

	case "gRPC":
		// создаем новый gRPC-сервис (обертку над обычным user сервисом) для обработки запросов от клиента
		userGRPC := grpcService.NewUserServiceGRPC(services.User)
		// создаем новый google gRPC-сервер, который будет использован в запуске пользовательского сервера через NewServerGRPC
		gRPCServer := grpc.NewServer()
		// регистрируем созданный gRPC-сервис userGRPC на сервере gRPCServer. Это связывает методы из userGRPC
		// с конкретными эндпоинтами на сервере gRPCServer, чтобы сервер мог обрабатывать запросы, поступающие от клиентов.
		grpcService.RegisterUsersServer(gRPCServer, userGRPC)
		// создаем пользовательский сервер для обработки gRPC-запросов, передавая в конструктор конфигурацию, экземпляр google gRPC-сервера и логгер.
		a.gRPC = server.NewServerGRPC(a.conf.RPCServer, gRPCServer, a.logger)
	}

	//инициализация хендлеров
	controllers := modules.NewControllers(services, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)

	// конфигурация http сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация http сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}

package modules

import (
	"go-grpc/gRPC_auth/internal/infrastructure/component"
	aservice "go-grpc/gRPC_auth/internal/modules/auth/service"
	uservice "go-grpc/gRPC_auth/internal/modules/user/service"
	"go-grpc/gRPC_auth/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

// Auth содержит и реализует два интерфейса: Userer и Auther.
// Это позволяет Auth сервису обрабатывать все операции, связанные с управлением пользователями и их аутентификацией и авторизацией.
func NewServices(storages *storages.Storages, userService uservice.Userer, components *component.Components) *Services {
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}

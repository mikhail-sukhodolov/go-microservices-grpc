package grpc

// реализация интерфейса сервера из сгенерированного auth.proto
// обертка над сервисом auth, вызываем обычные методы auth

import (
	"context"
	"go-grpc/gRPC_auth/internal/modules/auth/service"
)

type AuthServiceGRPC struct {
	auth service.Auther
}

func NewAuthServiceGRPC(authService service.Auther) *AuthServiceGRPC {
	return &AuthServiceGRPC{auth: authService}
}

func (a AuthServiceGRPC) Register(ctx context.Context, in *RegisterIn) (*RegisterOut, error) {
	out := a.auth.Register(ctx, service.RegisterIn{
		Email:          in.Email,
		Phone:          in.Phone,
		Password:       in.Password,
		IdempotencyKey: in.IdempotencyKey,
	})
	return &RegisterOut{
		Status:    int32(out.Status),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
func (a AuthServiceGRPC) AuthorizeEmail(ctx context.Context, in *AuthorizeEmailIn) (*AuthorizeOut, error) {
	out := a.auth.AuthorizeEmail(ctx, service.AuthorizeEmailIn{
		Email:    in.Email,
		Password: in.Password,
	})
	return &AuthorizeOut{
		UserID:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a AuthServiceGRPC) AuthorizeRefresh(ctx context.Context, in *AuthorizeRefreshIn) (*AuthorizeOut, error) {
	out := a.auth.AuthorizeRefresh(ctx, service.AuthorizeRefreshIn{
		UserID: int(in.UserID),
	})
	return &AuthorizeOut{
		UserID:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a AuthServiceGRPC) AuthorizePhone(ctx context.Context, in *AuthorizePhoneIn) (*AuthorizeOut, error) {
	out := a.auth.AuthorizePhone(ctx, service.AuthorizePhoneIn{
		Phone: in.Phone,
		Code:  int(in.Code),
	})
	return &AuthorizeOut{
		UserID:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a AuthServiceGRPC) SendPhoneCode(ctx context.Context, in *SendPhoneCodeIn) (*SendPhoneCodeOut, error) {
	out := a.auth.SendPhoneCode(ctx, service.SendPhoneCodeIn{
		Phone: in.Phone,
	})

	return &SendPhoneCodeOut{
		Phone: out.Phone,
		Code:  int32(out.Code),
	}, nil
}

func (a AuthServiceGRPC) VerifyEmail(ctx context.Context, in *VerifyEmailIn) (*VerifyEmailOut, error) {
	out := a.auth.VerifyEmail(ctx, service.VerifyEmailIn{
		Email: in.Email,
		Hash:  in.Hash,
	})
	return &VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

// не используется, если все методы реализованы
func (a AuthServiceGRPC) mustEmbedUnimplementedAutherServer() {
	panic("implement me")
}

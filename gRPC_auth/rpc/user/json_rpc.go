package Auth

import (
	"context"
	"go-grpc/gRPC_auth/internal/modules/auth/service"
)

// AuthServiceJSONRPC представляет AuthService для использования в JSON-RPC
type AuthServiceJSONRPC struct {
	AuthService service.Auther
}

// NewAuthServiceJSONRPC возвращает новый AuthServiceJSONRPC
func NewAuthServiceJSONRPC(AuthService service.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{AuthService: AuthService}
}

// Register обрабатывает JSON-RPC запрос на регистрацию пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод Register из AuthService.
// Возвращает выходные данные.
func (t *AuthServiceJSONRPC) Register(in service.RegisterIn, out *service.RegisterOut) error {
	*out = t.AuthService.Register(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) AuthorizeEmail(in service.AuthorizeEmailIn, out *service.AuthorizeOut) error {
	*out = t.AuthService.AuthorizeEmail(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) AuthorizeRefresh(in service.AuthorizeRefreshIn, out *service.AuthorizeOut) error {
	*out = t.AuthService.AuthorizeRefresh(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) AuthorizePhone(in service.AuthorizePhoneIn, out *service.AuthorizeOut) error {
	*out = t.AuthService.AuthorizePhone(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) SendPhoneCode(in service.SendPhoneCodeIn, out *service.SendPhoneCodeOut) error {
	*out = t.AuthService.SendPhoneCode(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) VerifyEmail(in service.VerifyEmailIn, out *service.VerifyEmailOut) error {
	*out = t.AuthService.VerifyEmail(context.Background(), in)
	return nil
}

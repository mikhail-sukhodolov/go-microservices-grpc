package grpc

import (
	"context"
	"go-grpc/gRPC_auth/internal/models"
	"go-grpc/gRPC_auth/internal/modules/user/service"
	"google.golang.org/grpc"
	"unsafe"
)

// в клиенте интерфейс из прото файла
type UserServiceGRPC struct {
	client UsersClient
}

func NewUserServiceGRPC(client grpc.ClientConnInterface) *UserServiceGRPC {
	newClient := NewUsersClient(client)
	return &UserServiceGRPC{client: newClient}
}

func (u *UserServiceGRPC) Create(ctx context.Context, in service.UserCreateIn) service.UserCreateOut {
	out, err := u.client.CreateUser(ctx, &UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int32(in.Role),
	})
	if err != nil {
		return service.UserCreateOut{
			UserID:    int(out.UserID),
			ErrorCode: int(out.ErrorCode),
		}
	}
	return service.UserCreateOut{
		UserID:    int(out.UserID),
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) Update(ctx context.Context, in service.UserUpdateIn) service.UserUpdateOut {
	fields := make([]int32, len(in.Fields))
	copy(fields, *(*[]int32)(unsafe.Pointer(&in.Fields)))
	out, err := u.client.Update(ctx, &UserUpdateIn{
		User: &User{
			ID:            int32(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int32(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: fields,
	})
	if err != nil {
		return service.UserUpdateOut{
			Success:   out.Success,
			ErrorCode: int(out.ErrorCode),
		}
	}
	return service.UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) VerifyEmail(ctx context.Context, in service.UserVerifyEmailIn) service.UserUpdateOut {
	out, err := u.client.VerifyEmail(ctx, &UserVerifyEmailIn{UserID: int32(in.UserID)})
	if err != nil {
		return service.UserUpdateOut{
			Success:   out.Success,
			ErrorCode: int(out.ErrorCode)}
	}
	return service.UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) ChangePassword(ctx context.Context, in service.ChangePasswordIn) service.ChangePasswordOut {
	out, err := u.client.ChangePassword(ctx, &ChangePasswordIn{
		UserID:      int32(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	})
	if err != nil {
		return service.ChangePasswordOut{
			Success:   out.Success,
			ErrorCode: int(out.ErrorCode),
		}
	}
	return service.ChangePasswordOut{
		Success:   out.Success,
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByEmail(ctx context.Context, in service.GetByEmailIn) service.UserOut {
	out, err := u.client.GetByEmail(ctx, &GetByEmailIn{Email: in.Email})
	if err != nil {
		return service.UserOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	return service.UserOut{
		User: &models.User{
			ID:            int(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByPhone(ctx context.Context, in service.GetByPhoneIn) service.UserOut {
	out, err := u.client.GetByPhone(ctx, &GetByPhoneIn{Phone: in.Phone})
	if err != nil {
		return service.UserOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	return service.UserOut{
		User: &models.User{
			ID:    int(out.User.ID),
			Name:  out.User.Name,
			Phone: out.User.Phone,
			Email: out.User.Email,
			Role:  int(out.User.Role),
		},
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByID(ctx context.Context, in service.GetByIDIn) service.UserOut {
	out, err := u.client.GetByID(ctx, &GetByIDIn{UserID: int32(in.UserID)})
	if err != nil {
		return service.UserOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	return service.UserOut{
		User: &models.User{
			ID:    int(out.User.ID),
			Name:  out.User.Name,
			Phone: out.User.Phone,
			Email: out.User.Email,
			Role:  int(out.User.Role),
		},
		ErrorCode: int(out.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByIDs(ctx context.Context, in service.GetByIDsIn) service.UsersOut {
	IDs := *(*[]int32)(unsafe.Pointer(&in.UserIDs))
	out, err := u.client.GetByIDs(ctx, &GetByIDsIn{UserIDs: IDs})
	if err != nil {
		return service.UsersOut{
			ErrorCode: int(out.ErrorCode),
		}
	}
	users := make([]models.User, len(out.User))
	for index, user := range out.User {
		users[index] = models.User{
			ID:            int(user.ID),
			Email:         user.Email,
			Password:      user.Password,
			Name:          user.Name,
			Phone:         user.Phone,
			EmailVerified: user.EmailVerified,
			PhoneVerified: user.PhoneVerified,
			Verified:      user.Verified,
			Role:          int(user.Role),
		}
	}
	return service.UsersOut{
		User:      users,
		ErrorCode: int(out.ErrorCode),
	}
}

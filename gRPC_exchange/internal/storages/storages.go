package storages

import (
	"go-grpc/gRPC_exchange/internal/db/adapter"
	"go-grpc/gRPC_exchange/internal/infrastructure/cache"
	"go-grpc/gRPC_exchange/internal/modules/exchange/storage"
)

type Storages struct {
	Exchange storage.Exchanger
}

//func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
//	return &Storages{Exchange: storage.NewExchangeStorage(sqlAdapter, cache)}
//}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{Exchange: storage.NewExchangeStorage(sqlAdapter, cache)}
}

package worker

import (
	"fmt"
	"github.com/streadway/amqp"
	"go-grpc/gRPC_exchange/config"
	"log"
)

type RabbitMQ struct {
	conf config.AppConf
}

func NewRabbitMQ(conf config.AppConf) *RabbitMQ {
	return &RabbitMQ{conf: conf}
}

func (r *RabbitMQ) RabbitWorker(body string) {
	// Создаем соединение с сервером RabbitMQ

	conn, err := amqp.Dial("amqp://guest:guest@" + r.conf.RabbitClient.Host + ":" + r.conf.RabbitClient.Port)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	// Создаем канал для работы с очередью
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	// Объявляем очередь, с которой будем работать
	q, err := ch.QueueDeclare(
		"queue", // Название очереди
		false,   // Указываем, что очередь не является персистентной
		false,   // Указываем, что очередь не является уникальной
		false,   // Указываем, что очередь не удаляется после последнего использования
		false,   // Указываем, что очередь не ждет подтверждения доставки сообщения
		nil,     // Аргументы для расширенной конфигурации (необязательно)
	)
	failOnError(err, "Failed to declare a queue")

	// Отправляем сообщения в очередь
	err = ch.Publish(
		"",     // Обменник (пустое значение используется для обработки сообщений в очереди)
		q.Name, // Название очереди
		true,   // Указываем, что сообщение является обязательным для доставки
		false,  // Указываем, что сообщение не является опцией возвращения (возвращается только в том случае, если нет активного потребителя)
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		},
	)
	failOnError(err, "Failed to publish a message")
	fmt.Println("Message sent successfully:", body)

}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

package worker

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"go-grpc/gRPC_exchange/internal/models"
	"go-grpc/gRPC_exchange/internal/modules"
	"go.uber.org/zap"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Worker struct {
	service modules.Services
	logger  *zap.Logger
	rabbit  RabbitMQ
}

func NewWorker(service *modules.Services, logger *zap.Logger, rabbit RabbitMQ) *Worker {
	return &Worker{service: *service, logger: logger, rabbit: rabbit}
}
func (w *Worker) Run() {
	ticker := time.NewTicker(5 * time.Second)
	samples := make(models.SampleRates)
	w.logger.Info("starting worker")
	go func() {
		for {
			select {
			// ticker.C представляет собой канал (chan) временных сигналоов
			// Когда сигнал от таймера приходит, блок case <-ticker.C: начинает выполняться
			case <-ticker.C:
				res, err := http.Post("https://api.exmo.com/v1.1/ticker",
					"application/x-www-form-urlencoded",
					bytes.NewBuffer([]byte{}))
				if err != nil {
					log.Fatal(err)
				}
				rates := make(models.Rates)
				err = json.NewDecoder(res.Body).Decode(&rates)
				if err != nil {
					log.Fatal(err)
				}
				w.service.Exchange.Update(context.Background(), rates)
				err = w.CheckSample(samples, rates)
				if err != nil {
					w.logger.Info(fmt.Sprintf("rabbit error: %v", err))
				}
			}
		}
	}()
}

func (w *Worker) CheckSample(samples models.SampleRates, rates models.Rates) error {
	for key, rate := range rates {
		// берем значение пары max/min СЕМПЛА по ключу (имени валюты)
		sample := samples[key]
		// присваиваем max и min семпла переменным
		sampleMax := sample.Max
		sampleMin := sample.Min

		//МАКСИМУМ
		// берем максимум текущей пары ТИКЕРА
		currentMax, err := strconv.ParseFloat(rate.High, 64)
		if err != nil {
			return err
		}
		//заполняем семпл
		if sampleMax == 0 {
			sample.Max = currentMax // обновляем значение max в семпле
			samples[key] = sample   // обновляем значения семпла в map

		} else if currentMax > sampleMax {
			// } else if currentMax > sampleMax+100 {
			w.rabbit.RabbitWorker(fmt.Sprintf("Новый максимум %v:%v", key, currentMax))
			sample.Max = currentMax // обновляем значение max в семпле
			samples[key] = sample   // обновляем значения семпла в map
		}

		//МИНИМУМ
		// берем минимум текущей пары ТИКЕРА
		currentMin, err := strconv.ParseFloat(rate.Low, 64)
		if err != nil {
			return err
		}
		//заполняем семпл
		if sampleMin == 0 {
			sample.Min = currentMin // обновляем значение min в семпле
			samples[key] = sample   // обновляем значения семпла в map

		} else if currentMin < sampleMin {
			// } else if currentMin < sampleMin-100 {
			w.rabbit.RabbitWorker(fmt.Sprintf("Новый минимум %v:%v", key, currentMin))
			sample.Min = currentMin // обновляем значение min в семпле
			samples[key] = sample   // обновляем значения семпла в map
		}
	}
	return nil
}

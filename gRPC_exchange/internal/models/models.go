package models

import "encoding/json"

func UnmarshalRates(data []byte) (Rates, error) {
	var r Rates
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Rates) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type ExchangeDTO struct {
	ID        int    `json:"id"  db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name      string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	BuyPrice  string `json:"buy_price" db:"buy_price" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	SellPrice string `json:"sell_price" db:"sell_price" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	LastTrade string `json:"last_trade" db:"last_trade" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	High      string `json:"high" db:"high" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Low       string `json:"low" db:"low" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Avg       string `json:"avg" db:"avg" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Vol       string `json:"vol" db:"vol" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	VolCurr   string `json:"vol_curr" db:"vol_curr" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Updated   int    `json:"updated" db:"update" db_type:"int" db_default:"not null" db_ops:"create,update"`
}

func (e *ExchangeDTO) TableName() string {
	return "ExchangeDTO"
}

func (e *ExchangeDTO) OnCreate() []string {
	return []string{}
}

type Rates map[string]Rate

type Rate struct {
	BuyPrice  string `json:"buy_price"`
	SellPrice string `json:"sell_price"`
	LastTrade string `json:"last_trade"`
	High      string `json:"high"`
	Low       string `json:"low"`
	Avg       string `json:"avg"`
	Vol       string `json:"vol"`
	VolCurr   string `json:"vol_curr"`
	Updated   int64  `json:"updated"`
}

func (r *Rate) TableName() string {
	return "Rate"
}

func (r Rate) OnCreate() []string {
	return []string{}
}

type MinPrice struct {
	Name string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Low  string `json:"low" db:"low" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}

func (m *MinPrice) TableName() string {
	return "MinPrice"
}

func (m *MinPrice) OnCreate() []string {
	return []string{}
}

type MaxPrice struct {
	Name string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	High string `json:"high" db:"high" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}

func (m *MaxPrice) TableName() string {
	return "MaxPrice"
}

func (m *MaxPrice) OnCreate() []string {
	return []string{}
}

type AvgPrice struct {
	Name string `json:"name"  db:"name" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
	Avg  string `json:"avg" db:"avg" db_type:"varchar(55)" db_default:"not null" db_ops:"create,update"`
}

type SampleRates map[string]SampleRate

type SampleRate struct {
	Max float64
	Min float64
}

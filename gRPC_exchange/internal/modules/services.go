package modules

import (
	"go-grpc/gRPC_exchange/internal/infrastructure/component"
	"go-grpc/gRPC_exchange/internal/modules/exchange/service"
	"go-grpc/gRPC_exchange/internal/storages"
)

type Services struct {
	Exchange service.Exchanger
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		Exchange: service.NewExchangeService(storages.Exchange, components.Logger)}
}

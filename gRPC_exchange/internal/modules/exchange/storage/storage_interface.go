package storage

import (
	"context"
	"go-grpc/gRPC_exchange/internal/models"
)

type Exchanger interface {
	Ticker(ctx context.Context) ([]models.ExchangeDTO, error)
	Update(ctx context.Context, rates models.Rates) error
	MinPrice(ctx context.Context) ([]models.MinPrice, error)
	MaxPrice(ctx context.Context) ([]models.MaxPrice, error)
	AvgPrice(ctx context.Context) ([]models.AvgPrice, error)
	History(ctx context.Context) ([]models.ExchangeDTO, error)
}

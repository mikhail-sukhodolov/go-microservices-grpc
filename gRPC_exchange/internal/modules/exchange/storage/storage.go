package storage

import (
	"context"
	"go-grpc/gRPC_exchange/internal/db/adapter"
	"go-grpc/gRPC_exchange/internal/infrastructure/cache"
	"go-grpc/gRPC_exchange/internal/models"
	"strings"
)

type ExchangeStorage struct {
	adapter *adapter.SQLAdapter
	cache   cache.Cache
}

func NewExchangeStorage(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) Exchanger {
	return &ExchangeStorage{adapter: sqlAdapter, cache: cache}
}

func (e *ExchangeStorage) Ticker(ctx context.Context) ([]models.ExchangeDTO, error) {
	var dtoList []models.ExchangeDTO
	err := e.adapter.List(ctx, &dtoList, "exchangedto", adapter.Condition{})
	if err != nil {
		return []models.ExchangeDTO{}, err
	}
	return dtoList, nil
}
func (e *ExchangeStorage) Update(ctx context.Context, rates models.Rates) error {
	for key, rate := range rates {
		// Преобразование данных из структуры Rate в структуры таблиц
		exchangeData := models.ExchangeDTO{
			Name:      key,
			BuyPrice:  rate.BuyPrice,
			SellPrice: rate.SellPrice,
			LastTrade: rate.LastTrade,
			High:      rate.High,
			Low:       rate.Low,
			Avg:       rate.Avg,
			Vol:       rate.Vol,
			VolCurr:   rate.VolCurr,
			Updated:   int(rate.Updated),
		}
		minData := models.MinPrice{
			Name: key,
			Low:  rate.Low,
		}
		maxData := models.MaxPrice{
			Name: key,
			High: rate.High,
		}
		var dataInserted bool
		// Попытка обновления записи в базе данных
		err := e.adapter.Update(ctx, &exchangeData, adapter.Condition{Equal: map[string]interface{}{"name": key}}, "update")
		if err != nil {
			// Если запись с ключом не найдена, то пытаемся вставить новую запись
			if strings.Contains(err.Error(), "no rows") {
				err = e.adapter.Create(ctx, &exchangeData)
				if err != nil {
					return err
				}
				err = e.adapter.Create(ctx, &minData)
				if err != nil {
					return err
				}
				err = e.adapter.Create(ctx, &maxData)
				if err != nil {
					return err
				}
				dataInserted = true

			} else {
				return err
			}
		}
		if !dataInserted {
			err = e.adapter.Update(ctx, &minData, adapter.Condition{Equal: map[string]interface{}{"name": key}}, "update")
			if err != nil {
				return err
			}
			err = e.adapter.Update(ctx, &maxData, adapter.Condition{Equal: map[string]interface{}{"name": key}}, "update")
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (e *ExchangeStorage) MinPrice(ctx context.Context) ([]models.MinPrice, error) {

	var minPrices []models.MinPrice
	selectQuery := "SELECT name, low FROM minprice"
	err := e.adapter.Db.SelectContext(ctx, &minPrices, selectQuery)
	if err != nil {
		return nil, err
	}
	return minPrices, nil
}

func (e *ExchangeStorage) MaxPrice(ctx context.Context) ([]models.MaxPrice, error) {

	var maxPrices []models.MaxPrice
	selectQuery := "SELECT name, high FROM maxprice"
	err := e.adapter.Db.SelectContext(ctx, &maxPrices, selectQuery)
	if err != nil {
		return nil, err
	}
	return maxPrices, nil
}

func (e *ExchangeStorage) AvgPrice(ctx context.Context) ([]models.AvgPrice, error) {

	var avgPrices []models.AvgPrice
	query := "SELECT name, avg FROM exchangedto"
	err := e.adapter.Db.SelectContext(ctx, &avgPrices, query)
	if err != nil {
		return nil, err
	}

	return avgPrices, nil
}

func (e *ExchangeStorage) History(ctx context.Context) ([]models.ExchangeDTO, error) {
	var dtoList []models.ExchangeDTO
	query := "SELECT * FROM exchangedto"

	err := e.adapter.Db.SelectContext(ctx, &dtoList, query)
	if err != nil {
		return []models.ExchangeDTO{}, err
	}
	return dtoList, nil
}

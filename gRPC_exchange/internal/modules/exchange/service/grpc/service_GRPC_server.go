package grpc

import (
	"context"
	"go-grpc/gRPC_exchange/internal/models"
	"go-grpc/gRPC_exchange/internal/modules/exchange/service"
)

type ExchangeServiceGRPC struct {
	exchange service.Exchanger
}

func NewExchangeServiceGRPC(exchangeService service.Exchanger) *ExchangeServiceGRPC {
	return &ExchangeServiceGRPC{exchange: exchangeService}
}

func (e ExchangeServiceGRPC) Ticker(ctx context.Context, request *TickerRequest) (*TickerResponse, error) {
	out := e.exchange.Ticker(ctx)
	res := make([]*ExchangeDTO, len(out.Exchanger))
	for i := range out.Exchanger {
		res[i] = &ExchangeDTO{
			Id:        int32(out.Exchanger[i].ID),
			Name:      out.Exchanger[i].Name,
			LastTrade: out.Exchanger[i].LastTrade,
		}
	}
	return &TickerResponse{
		ExchangeDTOs: res,
		ErrorCode:    int32(out.ErrorCode),
		Success:      out.Succses,
	}, nil

}

func (e ExchangeServiceGRPC) MinPrices(ctx context.Context, request *MinPricesRequest) (*MinPricesResponse, error) {
	// конвертируем структуру MinPrice в прото-структуру
	out := e.exchange.MinPrices(ctx)
	var res []*MinPrice
	for _, value := range out.Exchanger {
		value := &MinPrice{
			Name: value.Name,
			Low:  value.Low,
		}
		res = append(res, value)
	}
	return &MinPricesResponse{Success: out.Succses, ErrorCode: int32(out.ErrorCode), Rates: res}, nil
}

func (e ExchangeServiceGRPC) MaxPrices(ctx context.Context, request *MaxPricesRequest) (*MaxPricesResponse, error) {
	// конвертируем структуру MaxPrice в прото-структуру
	out := e.exchange.MaxPrices(ctx)
	var res []*MaxPrice
	for _, value := range out.Exchanger {
		value := &MaxPrice{
			Name: value.Name,
			High: value.High,
		}
		res = append(res, value)
	}
	return &MaxPricesResponse{Success: out.Succses, ErrorCode: int32(out.ErrorCode), Rates: res}, nil
}

func (e ExchangeServiceGRPC) AvgPrices(ctx context.Context, request *AvgPricesRequest) (*AvgPricesResponse, error) {
	out := e.exchange.AvgPrices(ctx)
	// конвертируем структуру AvgPrice в прото-структуру
	var res []*AvgPrice
	for _, value := range out.Exchanger {
		price := &AvgPrice{
			Name: value.Name,
			Avg:  value.Avg,
		}
		res = append(res, price)
	}
	return &AvgPricesResponse{Success: out.Succses, ErrorCode: int32(out.ErrorCode), Rates: res}, nil
}

func (e ExchangeServiceGRPC) History(ctx context.Context, request *HistoryRequest) (*HistoryResponse, error) {
	out := e.exchange.History(ctx)

	res := make([]*ExchangeDTO, len(out.History))
	for i := range out.History {
		res[i] = &ExchangeDTO{
			Id:        int32(out.History[i].ID),
			Name:      out.History[i].Name,
			LastTrade: out.History[i].LastTrade,
		}
	}

	return &HistoryResponse{
		ExchangeDTOs: res,
		ErrorCode:    int32(out.ErrorCode),
		Success:      out.Succses,
	}, nil
}

func (e ExchangeServiceGRPC) Update(ctx context.Context, request *UpdateRequest) (*UpdateResponse, error) {
	e.exchange.Update(ctx, models.Rates{})
	return &UpdateResponse{}, nil
}

func (e ExchangeServiceGRPC) mustEmbedUnimplementedExchangerServiceGRPCServer() {}

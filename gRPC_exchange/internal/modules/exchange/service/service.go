package service

import (
	"context"
	"go-grpc/gRPC_exchange/internal/models"
	"go-grpc/gRPC_exchange/internal/modules/exchange/storage"
	"go.uber.org/zap"
)

type ExchangeService struct {
	storage storage.Exchanger
	logger  *zap.Logger
}

func NewExchangeService(storage storage.Exchanger, logger *zap.Logger) *ExchangeService {
	return &ExchangeService{
		storage: storage,
		logger:  logger,
	}
}

func (e *ExchangeService) Update(ctx context.Context, rates models.Rates) {
	e.storage.Update(ctx, rates)
}
func (e *ExchangeService) Ticker(ctx context.Context) TickerOut {
	out, err := e.storage.History(ctx)
	if err != nil {
		return TickerOut{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return TickerOut{
		Exchanger: out,
		ErrorCode: 0,
		Succses:   true,
	}
}

func (e *ExchangeService) MinPrices(ctx context.Context) MinPriceOut {
	out, err := e.storage.MinPrice(ctx)
	if err != nil {
		return MinPriceOut{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return MinPriceOut{
		Exchanger: out,
		ErrorCode: 0,
		Succses:   true,
	}
}

func (e *ExchangeService) MaxPrices(ctx context.Context) MaxPriceOut {
	out, err := e.storage.MaxPrice(ctx)
	if err != nil {
		return MaxPriceOut{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return MaxPriceOut{
		Exchanger: out,
		ErrorCode: 0,
		Succses:   true,
	}
}

func (e *ExchangeService) AvgPrices(ctx context.Context) AvgPriceOut {
	out, err := e.storage.AvgPrice(ctx)
	if err != nil {
		return AvgPriceOut{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return AvgPriceOut{
		Exchanger: out,
		ErrorCode: 0,
		Succses:   true,
	}
}

func (e *ExchangeService) History(ctx context.Context) History {
	out, err := e.storage.History(ctx)
	if err != nil {
		return History{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return History{
		History:   out,
		ErrorCode: 0,
		Succses:   true,
	}
}

package service

import (
	"context"
	"go-grpc/gRPC_exchange/internal/models"
)

type Exchanger interface {
	Ticker(ctx context.Context) TickerOut
	Update(ctx context.Context, rates models.Rates)
	MinPrices(ctx context.Context) MinPriceOut
	MaxPrices(ctx context.Context) MaxPriceOut
	AvgPrices(ctx context.Context) AvgPriceOut
	History(ctx context.Context) History
}

type TickerOut struct {
	Exchanger []models.ExchangeDTO
	ErrorCode int
	Succses   bool
}

type MinPriceOut struct {
	Exchanger []models.MinPrice
	ErrorCode int
	Succses   bool
}

type MaxPriceOut struct {
	Exchanger []models.MaxPrice
	ErrorCode int
	Succses   bool
}

type AvgPriceOut struct {
	Exchanger []models.AvgPrice
	ErrorCode int
	Succses   bool
}

type History struct {
	History   []models.ExchangeDTO
	ErrorCode int
	Succses   bool
}

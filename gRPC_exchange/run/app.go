package run

import (
	"context"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"go-grpc/gRPC_exchange/config"
	"go-grpc/gRPC_exchange/internal/db"
	"go-grpc/gRPC_exchange/internal/infrastructure/cache"
	"go-grpc/gRPC_exchange/internal/infrastructure/component"
	"go-grpc/gRPC_exchange/internal/infrastructure/db/migrate"
	"go-grpc/gRPC_exchange/internal/infrastructure/db/scanner"
	"go-grpc/gRPC_exchange/internal/infrastructure/errors"
	"go-grpc/gRPC_exchange/internal/infrastructure/responder"
	"go-grpc/gRPC_exchange/internal/infrastructure/server"
	internal "go-grpc/gRPC_exchange/internal/infrastructure/service"
	"go-grpc/gRPC_exchange/internal/infrastructure/tools/cryptography"
	"go-grpc/gRPC_exchange/internal/models"
	"go-grpc/gRPC_exchange/internal/modules"
	grpcService "go-grpc/gRPC_exchange/internal/modules/exchange/service/grpc"
	"go-grpc/gRPC_exchange/internal/provider"
	"go-grpc/gRPC_exchange/internal/storages"
	"go-grpc/gRPC_exchange/internal/worker"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net/http"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	gRPC     server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt received", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	// запускаем созданный конструктором NewServerGRPC пользовательский сервер через метод Serve интерфейса Server
	errGroup.Go(func() error {
		err := a.gRPC.Serve(ctx)
		if err != nil {
			a.logger.Error("app: grpc server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}
	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.ExchangeDTO{},
		&models.MaxPrice{},
		&models.MinPrice{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	//инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}
	//инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Servises = services

	// создаем новый gRPC-сервис (обертку над обычным exchange сервисом) для обработки запросов от клиента
	exchangeGRPC := grpcService.NewExchangeServiceGRPC(services.Exchange)
	// создаем новый google gRPC-сервер, который будет использован в запуске пользовательского сервера через NewServerGRPC
	gRPCServer := grpc.NewServer()
	// регистрируем созданный gRPC-сервис exchangeGRPC на сервере gRPCServer. Это связывает методы из exchangeGRPC
	// с конкретными эндпоинтами на сервере gRPCServer, чтобы сервер мог обрабатывать запросы, поступающие от клиентов.
	grpcService.RegisterExchangerServiceGRPCServer(gRPCServer, exchangeGRPC)
	// создаем пользовательский сервер для обработки gRPC-запросов, передавая в конструктор конфигурацию, экземпляр google gRPC-сервера и логгер.
	a.gRPC = server.NewServerGRPC(a.conf.RPCServer, gRPCServer, a.logger)

	//запускаем воркер
	rabbit := worker.NewRabbitMQ(a.conf)
	work := worker.NewWorker(services, a.logger, *rabbit)
	work.Run()

	// конфигурация http сервера
	srv := &http.Server{
		Addr: fmt.Sprintf(":%s", a.conf.Server.Port),
	}
	// инициализация http сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
